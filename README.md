# AWS SSH - a tool to use "ssh" to access AWS SSM managed hosts

## Required software:
- [ ] Golang environment (macOS)
```
brew install go
```
# Build with make
```
make
```

# Usage:
```
╰─$ ./awssh -h
Usage of ./awssh:
  -p string
    	AWS profile name (default "default")
  -r string
    	AWS region (default "us-east-1")
  -v	Version
  ```

  # Example:
  ```
  ╰─$ ./awssh -p default web
/opt/homebrew/bin/aws ssm start-session --profile default --region us-east-1 --target i-deadbeefdeadbeef2 --document-name AWS-StartInteractiveCommand --parameters command="bash -l"

Starting session with SessionId: awesomeuser@somesite.com-f0a80183e104afa4
ssm-user@web:/var/snap/amazon-ssm-agent/6312$
```

## Committing to this repo
- [ ] Make commits like normal.
- [ ] Check the tag version
```
╰─$ git tag
v0.1
```
or
```
╰─$ git tag --sort=-version:refname | head -n 1                                                                          130 ↵
```
- [ ] Tag the code like:
```
git tag v0.2
```
- [ ] Run make to build
