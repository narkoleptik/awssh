package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"syscall"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

type instance struct {
	Name string
	ID   string
}

var version = "dev"

func main() {
	var instances []instance
	profile := ""
	region := ""
	awsExec := ""

	if _, err := os.Stat("/usr/local/bin/aws"); err == nil {
		awsExec = "/usr/local/bin/aws"
	} else if _, err := os.Stat("/usr/bin/aws"); err == nil {
		awsExec = "/usr/bin/aws"
	} else if _, err := os.Stat("/bin/aws"); err == nil {
		awsExec = "/usr/bin/aws"
	} else if _, err := os.Stat("/opt/homebrew/bin/aws"); err == nil {
		awsExec = "/opt/homebrew/bin/aws"
	} else {
		log.Fatal("Cannot find aws executable")
		os.Exit(1)
	}

	profileFlag := flag.String("p", "default", "AWS profile name")
	regionFlag := flag.String("r", "us-east-1", "AWS region")
	versionFlag := flag.Bool("v", false, "Version")

	flag.Parse()

	if *versionFlag {
		fmt.Println("Version:", version)
		os.Exit(0)
	}

	if *profileFlag != "" {
		profile = *profileFlag
	}

	if *regionFlag != "" {
		region = *regionFlag
	}

	if len(os.Args)%2 != 0 {
		fmt.Println("What would you like to search for?")
		os.Exit(1)
	}
	search := os.Args[len(os.Args)-1]

	svc := ec2.New(session.New(&aws.Config{
		Region:      aws.String(region),
		Credentials: credentials.NewSharedCredentials("", profile),
	}))

	searchFilter := fmt.Sprintf("%s*", search)
	input := &ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			{
				Name: aws.String("tag:Name"),
				Values: []*string{
					aws.String(searchFilter),
				},
			},
		},
	}

	result, err := svc.DescribeInstances(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return
	}

	for _, r := range result.Reservations {
		for _, i := range r.Instances {
			if *i.State.Name == "running" {
				for _, v := range i.Tags {
					if *v.Key == "Name" {
						var inst instance
						inst.Name = *v.Value
						inst.ID = *i.InstanceId
						instances = append(instances, inst)
					}
				}
			}
		}
	}

	host := ""

	sort.SliceStable(instances, func(i, j int) bool {
		return instances[i].Name < instances[j].Name
	})

	if len(instances) > 1 {
		valid := false
		for ok := true; ok; ok = !valid {
			for x, i := range instances {
				fmt.Printf("[%d] %s\n", x, i.Name)
			}
			fmt.Print("Your choice ([q]uit): ")
			option := ""
			fmt.Scanf("%s", &option)
			if option == "" {
				host = instances[0].ID
				valid = true
			} else if strings.ToLower(option) == "q" {
				os.Exit(0)
			} else {
				opt, err := strconv.Atoi(option)
				if (err == nil) && (opt < (len(instances))) {
					host = instances[opt].ID
					valid = true
				} else {
					fmt.Println("INVALID CHOICE:", option, "\n")
				}
			}
		}
	} else {
		if len(instances) > 0 {
			host = instances[0].ID
		} else {
			fmt.Println("There are no instances that match the search")
			os.Exit(1)
		}
	}

	var doneChannel = make(chan bool)

	signal.Ignore(syscall.SIGINT)

	go func() {

		fmt.Println(awsExec, "ssm", "start-session", "--profile", profile, "--region", region, "--target", host, "--document-name", "AWS-StartInteractiveCommand", "--parameters", "command=\"bash -l\"")
		cmd := exec.Command(awsExec, "ssm", "start-session", "--profile", profile, "--region", region, "--target", host, "--document-name", "AWS-StartInteractiveCommand", "--parameters", "command=\"bash -l\"")
		cmd.Stdout = os.Stdout
		cmd.Stdin = os.Stdin
		cmd.Stderr = os.Stderr
		cmd.Run()
		doneChannel <- true
	}()
	<-doneChannel
}
